
name := "test-task"
 
version := "1.0" 
      
lazy val `test-task` = (project in file(".")).enablePlugins(PlayJava, PlayAkkaHttp2Support)

scalaVersion := "2.12.6"

crossScalaVersions := Seq("2.11.12", "2.12.4")

val SpringVersion = "4.3.11.RELEASE"

libraryDependencies ++= Seq(
  "com.lightbend.play" %% "play-spring-loader" % "0.0.2",
  "org.springframework" % "spring-core" % SpringVersion,
  "org.springframework" % "spring-expression" % SpringVersion
)
// https://mvnrepository.com/artifact/org.postgresql/postgresql
libraryDependencies += "org.postgresql" % "postgresql" % "42.2.2"
// https://mvnrepository.com/artifact/org.springframework/spring-tx

libraryDependencies ++= Seq(
  "org.springframework" % "spring-orm" % SpringVersion,
  "org.hibernate" % "hibernate-core" % "5.2.12.Final",
  "org.springframework" % "spring-tx" % SpringVersion,
  "org.hibernate" % "hibernate-entitymanager" % "5.2.12.Final",
  "org.springframework" % "spring-aop" % SpringVersion

  //"org.springframework.data" % "spring-data-jpa" % "1.11.8.RELEASE"
)


// https://mvnrepository.com/artifact/c3p0/c3p0
libraryDependencies += "c3p0" % "c3p0" % "0.9.1.2"

// Testing libraries for dealing with CompletionStage...
libraryDependencies += "org.assertj" % "assertj-core" % "3.6.2" % Test
libraryDependencies += "org.awaitility" % "awaitility" % "2.0.0" % Test

// https://mvnrepository.com/artifact/org.mockito/mockito-core
libraryDependencies += "org.mockito" % "mockito-core" % "2.19.0" % Test

// Make verbose tests
testOptions in Test := Seq(Tests.Argument(TestFrameworks.JUnit, "-a", "-v"))
