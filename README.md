# README #

### Play framework app with spring loader ###
This small application makes use of the play framework to implement an Http server
running on akka server. spring-core is used for dependency, hibernate as orm.     
#

### How set up and test  the app ###
* clone the repository
* create and configure a postgresql database 
* import the dependencies 
* run the application  


#### Database setup ####
the db schema for the application is define as:

        create database "test-play";
        create table image
        (
          id  serial not null
            constraint image_pkey
            primary key,
          img bytea
        );

#### import dependencies ####
all dependency are set up in the `built.sbt` file 
  
### How to run tests ###

this app can be test in two way : 
* ##### testing the end points ####
    + `POST /images` create a new image
    + `GET /images/{imageId}` retrieve the image with id imageId
    + `GET /images/ids` return and array containing the ids of all the image in the bd  
    + `GET /images/{imageId}/thubmnails` return a byte array corresponding to the image cropped to be square 
* #### unit Testing the app ####

test written for this application are configure to work with `guice` as Dependency injection container
therefore to run test you need to:
+ add the `guice` dependency<pre>libraryDependencies += "com.google.inject" % "guice" % "4.2.0"
  </pre>
* replace the `@Autowired` annotation by `@Inject`
* remove the ` Spring loader configuration ` from the `application.conf`



