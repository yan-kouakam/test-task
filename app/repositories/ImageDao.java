package repositories;

import entities.Image;

import java.util.List;

public interface ImageDao {
    void addImage(Image img);
    Image getImageById(long id);
    List<Long> getAllIds();
    boolean deleteImageWithId(long id);
}
