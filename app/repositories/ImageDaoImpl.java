package repositories;

import entities.Image;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * Implementation of the dao repository
 *
 * @author Kouakam
 */
@Repository
public class ImageDaoImpl implements ImageDao {
    private SessionFactory sessionFactory;

    @Autowired
    public ImageDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public void addImage(Image img) {
        Session session = this.sessionFactory.getCurrentSession();
        Serializable id = session.save(img);
        System.out.println("id :"+ id.toString());
    }

    @Override
    @Transactional
    public Image getImageById(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.get(Image.class, id);
    }

    @Override
    @Transactional
    public List<Long> getAllIds() {
        Session session = this.sessionFactory.getCurrentSession();
         List result =  session.createQuery("select img.id from Image img")
                 .getResultList();
        return result;
    }

    @Override
    @Transactional
    public boolean deleteImageWithId(long id) {
        Session session  = this.sessionFactory.getCurrentSession();
        Image img = getImageById(id);
        if (img == null){
            //throw new Exception("image do not exits");
            return false;
        }
        session.delete(img);
        return true;
    }
}
