package utils;

import akka.actor.ActorSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import play.libs.concurrent.CustomExecutionContext;


//@Component
public class MyExecutionContext extends CustomExecutionContext {

    @Autowired
    public MyExecutionContext(ActorSystem actorSystem) {
        super(actorSystem, "akka.actor.default-dispatcher");
    }
}
