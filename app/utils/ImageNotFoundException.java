package utils;

public class ImageNotFoundException extends Exception{
    private String message;

    public ImageNotFoundException(String message) {
        this.message = message;
    }
}
