package config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.typesafe.config.Config;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

/**
 * configuration of spring dependency injection
 * define beans needed by the spring container
 * @author Kouakam
 */
@Configuration
@ComponentScan(basePackages = {"controllers", "repositories","entities","utils","play.mvc"})
@EnableTransactionManagement
public class PlaySpringDependencyInjectionConfiguration {

    /**
     *creates an instance of postgres datasource
     */
    @Bean
    DataSource dataSource(Config config) {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        String url = config.getString("db.default.url");
        String driver = config.getString("db.default.driver");
        String user = config.getString("db.default.username");
        String pass = config.getString("db.default.password");
        try {
            dataSource.setJdbcUrl(url);
            dataSource.setDriverClass(driver);
            dataSource.setUser(user);
            dataSource.setPassword(pass);
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        return dataSource;
    }

    /**
     * Hibernate session factory bean
     * @param dataSource an instance of a postgres datasource connection
     * @param config application configuration
     * @return a session factory bean
     */

    @Bean
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource, Config config){
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource);
        sessionFactoryBean.setPackagesToScan("entities");

        sessionFactoryBean.setHibernateProperties(getHibernateProperties(config));
        return sessionFactoryBean;
    }

    /***
     * transaction manager bean
     * @param sessionFactory the current session factory
     * @return a transaction manager
     */
    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory){
        HibernateTransactionManager tx = new HibernateTransactionManager();
        tx.setSessionFactory(sessionFactory);
        return tx;
    }

    /**
     * utility methods
     * retrieves the hibernate configuration properties from the configuration file
     * @param config an instance of the Config class containing a Map<Key ,Value> of configuration
     * @return Properties object
     */
    private Properties getHibernateProperties(Config config){

        Properties props = new Properties();
        props.setProperty("hibernate.dialect", config.getString("db.default.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", config.getString("db.default.hibernate.show_sql"));
        props.setProperty("hibernate.format_sql", config.getString("db.default.hibernate.format_sql"));
        props.setProperty("hibernate.hibernate.connection.autocommit", config.getString("db.default.hibernate.connection.autocommit"));
        return props;
    }

//    @Bean
//    @Autowired
//    CustomExecutionContext myExecutionContext(ActorSystem actor){
//        return new MyExecutionContext(actor);
//    }

}
