package controllers;

import akka.stream.javadsl.Source;
import akka.util.ByteString;
import entities.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import play.http.HttpEntity;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.* ;
import repositories.ImageDao;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 *  This class contains the action for creating, retrieve, update, deleting images
 * @author Kouakam
 */

@Component
public class ImageController extends Controller {
    /**
     * Image repository interface, provides a set of functions to
     * handle JDBC operations
     */
    private ImageDao imageDao;
    //private CustomExecutionContext context;
    private HttpExecutionContext context;

    public ImageController() {
    }

    @Autowired
    public ImageController(ImageDao imageDao , HttpExecutionContext ctx) {
        this.imageDao = imageDao;
        this.context = ctx;
    }

    /**
     * Action that get an image as byte array in the request body and save it in the database
     * the image should be load as multipart/form-data not a json object
     * @return Http.STATUS_OK and an empty response body
     */
    public CompletionStage<Result> saveImage(){
        // retrieve the body  of the request in an async fashion

        // retrieve the file from the request as multi-part-form
        return CompletableFuture.supplyAsync(()->{

            // get the first file in the request body,
            // this is use to avoid any constraint on the key-value pair  use to send the file in the request body
            try {
                Http.MultipartFormData<File> body = request().body().asMultipartFormData();
                Http.MultipartFormData.FilePart<File> image = body.getFiles().get(0);
                if (image != null) {
                    File file = image.getFile();

                    InputStream in = new FileInputStream(file);
                    byte[] img = new byte[(int)file.length()];
                    int  count = in.read(img);
                    Image image1 = new Image(img);
                    this.imageDao.addImage(image1);
                 //return an instance of the Result object as a promise to the completion stage
                    return new Result( new ResponseHeader(Http.Status.CREATED, Collections.emptyMap()),
                                       new HttpEntity.Strict(ByteString.fromString("success"), Optional.of("application/json")));

                }else {
                    return badRequest("File Missing or not correctly upload");
                }
            } catch (Exception e) {
                // if the image is not save return a response object with the exception message as body
                return new Result( new ResponseHeader(Http.Status.UNPROCESSABLE_ENTITY, Collections.emptyMap()),
                        new HttpEntity.Strict(ByteString.fromString(e.getMessage()), Optional.of("application/json")));
            }
        }, context.current());

    }
    /**
     * simple implementation of saving an image with blocking IO
     */

//    public Result saveImage(){
//        // retrieve the file from the request as multi-part-form
//        Http.MultipartFormData<File> body = request().body().asMultipartFormData();
//        // get the first file in the request body, this is use to avoid any constraint on the key-value pair  use to send the file in the request body
//        Http.MultipartFormData.FilePart<File> image = body.getFiles().get(0);
//        if (image != null) {
//            File file = image.getFile();
//            try {
//                InputStream in = new FileInputStream(file);
//                byte[] img = new byte[(int)file.length()];
//                int  count = in.read(img);
//                Image image1 = new Image(img);
//                this.imageDao.addImage(image1);
//                return ok("File uploaded");
//            } catch (Exception e) {
//                return badRequest(e.getMessage());
//            }
//        } else {
//            return badRequest("File Missing or not correctly upload");
//        }
//    }
    /**
     * Retrieves the image with the id specified
     * @param id the primary key of the image to fetch
     * @return Http.STATUS_OK if the image exist and ttp.STATUS_NOT_FOUND if not
     */
    public Result getImage(long id){
       //
        CompletionStage<Image> stage = CompletableFuture.supplyAsync(()->this.imageDao.getImageById(id), context.current());
        try {
            Image image = ((CompletableFuture<Image>) stage).get();
             if(image == null) {
                 throw new Exception(String.format("image with id = %d not found", id));
             }

            return ok(image.getImg());

        } catch ( Exception e) {
            return notFound("image with id = " + id + " not found");
        }

        /*
            CompletionStage<Image> stage = CompletableFuture.supplyAsync(()->this.imageDao.getImageById(id),context.current());
            return stage.thenApplyAsync((image) ->{
                if (image == null){
                    try {
                        throw new Exception(String.format("image with id = %d not found", id));
                    } catch (Exception e) {
                        return new Result(
                                new ResponseHeader(Http.Status.NOT_FOUND, Collections.emptyMap()),
                                new HttpEntity.Strict(ByteString.fromString(e.getMessage()),Optional.empty()));
                    }
                }

                return new Result(null,null);
            }, context.current());

     */
    }

    /**
     * Retrieves the list of id
     * @return List containing all the id of images in the database
     */
    public Result getIds(){
        List<Long> ids = this.imageDao.getAllIds();
       return ok(Json.toJson(ids));
    }

    /**
     * deletes the image from the database
     * @param id primary key of the image to be deleted
     * @return Http.STATUS_OK if the image exist and ttp.STATUS_NOT_FOUND if not
     */
    public Result deleteImage(long id){

        try {
            boolean imageDeleted = this.imageDao.deleteImageWithId(id);
            if (!imageDeleted){
                throw new Exception(String.format("image with id = %d not found", id));
            }

        } catch (Exception e) {
            return notFound(e.getMessage());
        }
        return ok();
    }

    /**
     * retrieves a thumbnail of and image from the db
     * This action make an asynchronous call to the bd to retrieve the image
     * @param id primary key of the image to retrieve
     * @return a cropped image
     */
    public CompletionStage<Result> getThumbnail(long id){
        /*TODO
        * make and async jdbc call to retrieve the img and crop it
        * computer the content type length to avoid loading the image in memory
        * send the response once all the computation are done
        * */

        try {
            // send async request to retrieve the image
            Image img = CompletableFuture.supplyAsync(()->this.imageDao.getImageById(id),context.current()).get();

            if(img == null) {
                throw new Exception(String.format("image with id = %d not found", id));
            }
            // return a promise of and object of type Result
            return cropImage(img.getImg()).thenApplyAsync(byteImage ->{

                // calculate the content-length of the response of the response to avoid that the image been load in memory entirely
                Optional<Long> contentLength = Optional.of((Long.parseLong(""+byteImage.length+"")));
                // prepare a source to stream each part of the image once it is ready
                Source<ByteString, ?> source = Source.single(byteImage).map(ByteString::fromArray);
                // return the image as a stream
                return new Result(
                        new ResponseHeader(200, Collections.emptyMap()),
                        new HttpEntity.Streamed(source, contentLength , Optional.of("multipart/form-data"))
                );

                /*
                  NB: important remarks
                  if the image need to be render as byte[] instead of and encoded Bytestring , comments the return statement above
                  and uncomment the line of code below, be aware that this will cause the image to be load in memory
                  so that the content-length can be compute  therefore cant lead to performance issues
                */

                //return ok(byteImage);
            }, context.current());
        } catch (Exception e) {
            return CompletableFuture.completedFuture(notFound(e.getMessage()));
       }
    }

    /**
     * utility method use to crop the image
     * use a promise to handle the blocking nature of IO operation in stream
     * @param img image to be cropped
     * @return a promise of a byte array containing the cropped image
     * @throws IOException throws and IOException
     */
    private static CompletionStage<byte[]> cropImage(byte[] img) throws IOException{

        //transform byte array into an buffer image to be able to retrieve the height and width
        InputStream inputStream = new ByteArrayInputStream(img);
        BufferedImage bufferedImage =  ImageIO.read(inputStream);
        int height = bufferedImage.getHeight();
        int width = bufferedImage.getWidth();
        // get the min of the height and width, this is use as the size to crop the image
        int size = height <= width ? height:width ;
        // crop the image to be square
        bufferedImage = bufferedImage.getSubimage(0, 0, size, size);
        OutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage,"jpeg", outputStream);
        outputStream.flush();
        byte [] result  = ((ByteArrayOutputStream) outputStream).toByteArray();
        outputStream.close();

        return  CompletableFuture.completedFuture(result);
    }

}
