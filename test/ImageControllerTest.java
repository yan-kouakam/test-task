import akka.util.ByteString;
import controllers.routes;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;
import play.mvc.Call;
import play.mvc.Http;
import play.mvc.Http.*;
import play.mvc.Result;
import play.Application;
import play.test.Helpers;
import play.test.WithApplication;
import repositories.ImageDao;
import repositories.ImageDaoImpl;


import static play.test.Helpers.*;
import static org.junit.Assert.*;

public class ImageControllerTest extends WithApplication {
    private Application application;
    private ImageDao dao;


    @Before
    public void setApp(){
        this.application =fakeApplication();
        this.dao = mock(ImageDaoImpl.class);
    }

    public ImageControllerTest() {
    }

    @Test
    public void saveImage() {
        fail("This is not yet implemented");
    }

    @Test
    public void getImage() {

        running(this.application, ()-> {
            Result res = route(this.application,
                    fakeRequest( controllers.routes.ImageController.getImage(1)));
            assertEquals(Status.NOT_FOUND, res.status());
             res = route(this.application,
                    fakeRequest( controllers.routes.ImageController.getImage(3)));
             assertEquals(Status.OK, res.status());
        });
    }

    @Test
    public void getIds() {

        running(this.application, ()->{

            Result result = route(this.application, fakeRequest(
                    controllers.routes.ImageController.getIds()
            ));

            assertEquals(result.status(), OK);
            assertEquals(contentAsBytes(result), ByteString.fromString("[2, 3, 4]"));
        });
    }

    @Test
    public void deleteImage() {
        running(this.application, ()-> {
            Result res = route(this.application,
                               fakeRequest( controllers.routes.ImageController.deleteImage(1)));
            assertEquals(Status.ACCEPTED,res.status());
        });
    }

    /**
     * test all the route in the image controller to be valid
     * also test and invalid route
     */
    @Test
    public void testRoutes(){
        RequestBuilder request = fakeRequest()
                .method(GET)
                .uri("/image/thumbnail");

        //  test /images/ids is valid and has the method GET
        Call call = controllers.routes.ImageController.getIds();
        assertEquals(call.url(), "/images/ids");
        assertEquals("this check the method use for this url",call.method(), GET);

        // test /images is valid and has a POST method
        call = controllers.routes.ImageController.saveImage();
        assertEquals(call.url(), "/images");
        assertEquals(call.method(), POST);

        Result result =  route(this.application, request);
        assertEquals(Http.Status.NOT_FOUND, result.status());
    }

}